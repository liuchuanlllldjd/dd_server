FROM node:18.16.0

ARG RUNNING

# 设置时区
ENV TZ=Asia/Shanghai DEBIAN_FRONTEND=noninteractive
ENV RUNNING $RUNNING
# 创建工作目录
RUN mkdir /app

# 指定工作目录
WORKDIR /app

# 复制当前所有代码到/app工作目录
COPY . .

#install
RUN yarn

# 打包
RUN yarn build
RUN echo $RUNNING

# 运行
CMD if [ -z "$RUNNING" ] ; then echo '未传递环境变量'; else yarn run:$RUNNING; fi
#暴露端口3700（与服务启动端口一致）
EXPOSE 3700
EXPOSE 4000
