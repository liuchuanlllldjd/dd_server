import { Module } from '@nestjs/common';
import { GroupMsgService } from './group-msg.service';
import { GroupMsgController } from './group-msg.controller';

@Module({
  controllers: [GroupMsgController],
  providers: [GroupMsgService]
})
export class GroupMsgModule {}
