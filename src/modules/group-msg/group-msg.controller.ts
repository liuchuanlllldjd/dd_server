import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { GroupMsgService } from './group-msg.service';

@Controller('group-msg')
export class GroupMsgController {
  constructor(private readonly groupMsgService: GroupMsgService) {}
}
