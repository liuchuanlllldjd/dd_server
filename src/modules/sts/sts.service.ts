import { Injectable } from '@nestjs/common';

@Injectable()
export class StsService {
  create(createStDto) {
    return 'This action adds a new st';
  }

  findAll() {
    return `This action returns all sts`;
  }

  findOne(id: number) {
    return `This action returns a #${id} st`;
  }

  update(id: number, updateStDto) {
    return `This action updates a #${id} st`;
  }

  remove(id: number) {
    return `This action removes a #${id} st`;
  }
}
