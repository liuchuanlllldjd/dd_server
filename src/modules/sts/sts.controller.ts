import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { StsService } from './sts.service';

@Controller('sts')
export class StsController {
  constructor(private readonly stsService: StsService) {}
  @Post()
  create(@Body() createStDto) {
    return createStDto;
  }
}
