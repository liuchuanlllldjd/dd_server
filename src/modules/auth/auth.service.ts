import { Injectable, HttpException, Inject } from '@nestjs/common';
import { User } from 'src/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/modules/user/user.service';
import { compareSync } from 'bcryptjs';
import { SocketService } from 'src/modules/socket/socket.service';
import { SocketGateway } from '../socket/socket.gateway';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private socketService: SocketService,
    private ws: SocketGateway,
  ) {}

  async validate(username: string, password: string): Promise<User> {
    const hasUser = await this.userService.findOne({ username });
    if (!hasUser.password) {
      throw new HttpException('该用户未注册', 400);
    }
    // 注：实际中的密码处理应通过加密措施
    if (!compareSync(password, hasUser.password)) {
      throw new HttpException('密码错误', 400);
    }
    return hasUser;
  }

  async login(user: User): Promise<any> {
    const { username } = user;
    const { password, ...others } = await this.userService.findOne({
      username,
    });
    const isLogin = await this.socketService.haveSet(others.id);
    console.log('isLogin', isLogin);
    if (isLogin)
      return {
        isLogin: true,
      };
    return {
      token: this.jwtService.sign(others),
    };
  }

  async kickOff(user: User): Promise<any> {
    const { username } = user;
    const hasUser = await this.userService.findOne({ username });
    await this.ws.handleKickOff(hasUser);
  }
  async logout(id: number): Promise<any> {
    const hasUser = await this.userService.findOne({ id });
    console.log('hasUser', hasUser);
    await this.ws.handleLogout(hasUser);
  }
}
