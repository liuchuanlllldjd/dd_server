import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PassportModule } from '@nestjs/passport';
import { UserModule } from 'src/modules/user/user.module';
import { JwtModule } from '@nestjs/jwt';
import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt.strategy';
import { ConfigModule } from '@nestjs/config';
import { jwtConstants } from './constants';
import { Server } from 'socket.io';
import { MessageService } from '../message/message.service';
import { Message } from 'src/entities/message.entity';
import { MessageController } from '../message/message.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SocketModule } from '../socket/socket.module';
@Module({
  imports: [
    TypeOrmModule.forFeature([Message]),
    ConfigModule,
    UserModule,
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1d' },
    }),
    SocketModule,
  ],
  exports: [AuthService],
  providers: [AuthService, LocalStrategy, JwtStrategy, MessageService, Server],
  controllers: [AuthController, MessageController],
})
export class AuthModule {}
