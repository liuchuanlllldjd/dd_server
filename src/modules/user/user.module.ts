import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { User } from 'src/entities/user.entity';
import { Friend } from 'src/entities/friend.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FriendService } from '../friend/friend.service';
import { SocketModule } from '../socket/socket.module';
@Module({
  imports: [TypeOrmModule.forFeature([User, Friend]), SocketModule],
  exports: [UserService],
  controllers: [UserController],
  providers: [UserService, FriendService],
})
export class UserModule {}
