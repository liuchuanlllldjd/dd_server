import { DocumentBuilder } from '@nestjs/swagger';
import { Injectable } from '@nestjs/common';
import { HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository, FindOptionsWhere, Not } from 'typeorm';
import {
  CreateUserDto,
  FindUserDto,
  FindUserListDto,
  UpdateUserDto,
} from 'src/dto/user.dto';
import { hashSync } from 'bcryptjs';
import { TypeOrmUtils } from 'src/utils/TypeOrmUtils';
import { isNormalInteger } from 'src/utils/isNumber';
import { FriendService } from '../friend/friend.service';
import { User } from 'src/entities/user.entity';
import { Friend } from 'src/entities/friend.entity';
import { SocketService } from 'src/modules/socket/socket.service';
import { throwError } from 'rxjs';

// hashSync(密码，加密盐)加密
//compareSync(传入的数据,数据库的数据)判断是否相等
@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Friend)
    private readonly friendRepository: Repository<Friend>,
    private readonly friendService: FriendService,
    private readonly socketService: SocketService,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const { username, password } = createUserDto;

    // if (!isNormalInteger(username))
    //   throw new HttpException('请输入正确的账号格式', 400);

    const hasUser = await this.findOne({ username });
    if (hasUser.id) {
      throw new HttpException('该用户已注册', 400);
    }

    createUserDto.password = hashSync(password, 10);
    const user = this.userRepository.create(createUserDto);
    this.userRepository.save(user);
    return { message: '注册成功' };
  }

  async logout(id: string) {
    await this.socketService.removeClient(id);
    return { message: '退出成功' };
  }

  async findOne(findUser: FindUserDto, user?: User) {
    const hasUser = await this.userRepository.findOne({
      where: findUser,
    });
    if (user) {
      if (user.id === hasUser.id) return { ...hasUser };
      const { state } = await this.friendRepository.findOne({
        where: {
          userId: user.id,
          friendId: hasUser.id,
        },
      });
      return { ...hasUser, state };
    } else return { ...hasUser };
  }

  async findAll(findUser: FindUserListDto, user) {
    const { keyword, gender, page, pageSize } = findUser;
    try {
      const [list, total] = await this.userRepository.findAndCount({
        // relations: ['friends'],
        select: [
          'createdAt',
          'email',
          'gender',
          'id',
          'imgUrl',
          'nickname',
          'phone',
          'username',
        ],
        where: [
          {
            nickname: TypeOrmUtils.like(keyword),
            id: Not(user.id),
          },
          { username: TypeOrmUtils.like(keyword), id: Not(user.id) },
        ],
        order: {
          id: 'ASC',
        },
        skip: (page - 1) * pageSize,
        take: pageSize,
      });
      const getState = async (list, userId) => {
        const items = [];
        for (let i = 0; i < list.length; i++) {
          const friend = await this.friendRepository.findOne({
            where: {
              userId,
              friendId: list[i].id,
            },
          });
          let state = 0;
          if (friend) state = friend.state;
          items.push({ ...list[i], state });
        }
        return items;
      };
      const items = await getState(list, user.id);
      return {
        items,
        total,
      };
    } catch (error) {
      throw new HttpException(error, 500);
    }
  }

  async updateUser({ id, ...others }: UpdateUserDto) {
    const user = await this.findOne({ id });
    const newUser = Object.assign(user, others);
    await this.userRepository.save(newUser);
    return '修改成功';
  }

  async deleteUser(id: number) {
    const { affected } = await this.userRepository.delete(id);
    if (affected) return '删除成功';
    throw new HttpException('无该用户', 400);
  }
}
