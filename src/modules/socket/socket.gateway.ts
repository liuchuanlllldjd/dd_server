import { findAllMessageDto } from './../../dto/message.dto';
import { MessageService } from './../message/message.service';
import {
  WebSocketGateway,
  SubscribeMessage,
  WebSocketServer,
} from '@nestjs/websockets';
import { SocketService } from './socket.service';
import { Server } from 'socket.io';
import { CreatedMessageDto } from 'src/dto/message.dto';
import { Socket } from 'socket.io';
import { User } from 'src/entities/user.entity';

@WebSocketGateway(4000, { cors: true })
export class SocketGateway {
  @WebSocketServer() server: Server;
  private readonly clients = new Map<string, Socket>();
  constructor(
    private readonly socketService: SocketService,
    private readonly messageService: MessageService,
  ) {}

  @SubscribeMessage('connection')
  async handleConnection(client: Socket, id: number) {
    console.log('id', id);
    if (id) await this.socketService.addClient(id, client);
  }

  @SubscribeMessage('kickOff')
  async handleKickOff(user: User) {
    const socketId = this.socketService.getClientById(user.id)?.id;
    this.server.to(socketId).emit('kickOffed', { message: '踢下线' });
    this.socketService.removeClient(String(user.id));
  }

  @SubscribeMessage('logout')
  async handleLogout(user: User) {
    this.socketService.removeClient(String(user.id));
  }

  // 获取消息列表
  @SubscribeMessage('getMessageList')
  async message(client: Socket, createMessage: findAllMessageDto) {
    return this.messageService.findAll(
      createMessage.userId,
      createMessage.friendId,
      createMessage.skip,
      createMessage.take,
    );
  }

  // 发送消息
  @SubscribeMessage('addMessage')
  async addMessage(client: any, createMessage: CreatedMessageDto) {
    await this.messageService.create(createMessage);
    const socketId = this.socketService.getClientById(createMessage.userId)?.id;
    const socket = this.socketService.getClientById(createMessage.friendId);
    const message = await this.messageService.findLatest(
      createMessage.userId,
      createMessage.friendId,
    );
    this.server.to(socketId).emit('message', { message });
    if (socket) {
      this.server.to(socket.id).emit('message', { message });
    }
  }
}
