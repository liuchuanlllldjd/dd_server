import { Injectable } from '@nestjs/common';
import { Socket, Server } from 'socket.io';

@Injectable()
export class SocketService {
  private readonly clients = new Map<string, Socket>();
  constructor(private io: Server) {}
  addClient(userId: number, client: Socket) {
    if (userId === undefined) return;
    return this.clients.set(String(userId), client);
  }

  getClientById(userId: number): Socket | undefined {
    return this.clients.get(String(userId));
  }
  haveSet(userId: number) {
    return this.clients.has(String(userId));
  }
  removeClient(userId: string) {
    return this.clients.delete(userId);
  }
}
