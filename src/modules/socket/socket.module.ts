import { Message } from 'src/entities/message.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MessageController } from './../message/message.controller';
import { MessageService } from './../message/message.service';
import { Module } from '@nestjs/common';
import { SocketService } from './socket.service';
import { SocketGateway } from './socket.gateway';
import { Server } from 'socket.io';
import { UserService } from '../user/user.service';
import { User } from 'src/entities/user.entity';
import { Friend } from 'src/entities/friend.entity';
import { FriendService } from '../friend/friend.service';

@Module({
  imports: [TypeOrmModule.forFeature([Message, User, Friend])],
  exports: [SocketService, SocketGateway],
  controllers: [MessageController],
  providers: [
    SocketGateway,
    SocketService,
    MessageService,
    Server,
    UserService,
    FriendService,
  ],
})
export class SocketModule {}
