import { FindFriendDto } from './../../dto/friend.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Injectable, HttpException } from '@nestjs/common';
import { Friend } from 'src/entities/friend.entity';
import { State } from 'src/entities/friend.entity';
@Injectable()
export class FriendService {
  constructor(
    @InjectRepository(Friend)
    private readonly friendRepository: Repository<Friend>,
  ) {}

  async create(userId, friendId) {
    const hasFriend = await this.findOne({ userId, friendId });
    const hasFriend1 = await this.findOne({
      userId: friendId,
      friendId: userId,
    });
    if (hasFriend || hasFriend1) throw new HttpException('已是好友', 400);

    const friend = await this.friendRepository.create({
      userId,
      friendId,
      state: 1,
    });
    const friend1 = await this.friendRepository.create({
      userId: friendId,
      friendId: userId,
      state: 2,
    });
    const res = await this.friendRepository.save([friend, friend1]);
    return res;
  }

  async passFriend(userId, friendId) {
    try {
      this.friendRepository.update({ userId, friendId }, { state: 3 });
      this.friendRepository.update(
        { friendId: userId, userId: friendId },
        { state: 3 },
      );
    } catch (error) {
      throw new HttpException('更新失败', 400);
    }
    return { msg: '通过' };
  }

  async findAll(findFriend: FindFriendDto, keyword: string) {
    const [list, total] = await this.friendRepository.findAndCount({
      relations: ['user'],
      where: findFriend,
      order: {
        id: 'DESC',
      },
    });
    const items = list.map((i) => {
      return { ...i.user };
    });
    return {
      items,
      total,
    };
  }

  async findOne(findFriend: FindFriendDto) {
    return await this.friendRepository.findOneBy(findFriend);
  }

  async findFriends(id: number) {
    const [items, total] = await this.friendRepository.findAndCount({
      relations: ['friend'],
      where: { userId: id, state: State.IS_FRIEND + 1 },
      order: {
        id: 'DESC',
      },
    });

    return {
      items,
      total,
    };
  }
}
