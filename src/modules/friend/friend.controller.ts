import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Request,
  Query,
} from '@nestjs/common';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FriendService } from './friend.service';
import { ParseIntPipe, DefaultValuePipe } from '@nestjs/common';

@Controller('friend')
export class FriendController {
  constructor(private readonly friendService: FriendService) {}
  @Post()
  create(@Request() request, @Body('friendId') id: number) {
    return this.friendService.create(request.user.id, id);
  }

  @Get('getState')
  getState(@Request() request, @Query('friendId') friendId: number) {
    return this.friendService.findOne({ userId: request.user.id, friendId });
  }

  @Post('passFriend')
  passFriend(@Request() request, @Body('friendId') friendId) {
    return this.friendService.passFriend(request.user.id, friendId);
  }

  @Get()
  findAll(@Request() request) {
    return this.friendService.findFriends(request.user.id);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.friendService.findOne({ id });
  }
}
