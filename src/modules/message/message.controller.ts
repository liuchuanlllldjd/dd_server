import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Query,
  Request,
} from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { CreatedMessageDto, findAllDto } from 'src/dto/message.dto';
import { MessageService } from './message.service';

@Controller('message')
export class MessageController {
  constructor(private readonly messageService: MessageService) {}

  @Post()
  @ApiOperation({ summary: '发送消息' })
  create(@Body() createdMessage: CreatedMessageDto) {
    return this.messageService.create(createdMessage);
  }

  @Get()
  @ApiOperation({ summary: '获取消息列表' })
  findAll(@Query() createdMessage: findAllDto, @Request() request) {
    return this.messageService.findAll(
      request.user.id,
      createdMessage.friendId,
      createdMessage.skip,
      createdMessage.take,
    );
  }
}
