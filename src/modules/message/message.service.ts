import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreatedMessageDto } from 'src/dto/message.dto';
import { Message } from 'src/entities/message.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(Message)
    private readonly messageRepository: Repository<Message>,
  ) {}

  async create(createMessage: CreatedMessageDto) {
    const message = await this.messageRepository.create(createMessage);
    const res = await this.messageRepository.save(message);
    return res;
  }

  async findAll(userId: number, friendId: number, skip: number, take: number) {
    const [items] = await this.messageRepository.findAndCount({
      where: [
        {
          userId,
          friendId,
        },
        { friendId: userId, userId: friendId },
      ],
      order: {
        createdAt: 'DESC',
      },
      skip,
      take,
    });
    items.reverse();
    return {
      items,
    };
  }
  async findLatest(userId: number, friendId: number) {
    const [items] = await this.messageRepository.findAndCount({
      where: [
        {
          userId,
          friendId,
        },
        { friendId: userId, userId: friendId },
      ],
      order: {
        createdAt: 'DESC',
      },
      skip: 0,
      take: 1,
    });
    return items[0];
  }
}
