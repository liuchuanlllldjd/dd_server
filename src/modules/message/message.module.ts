import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { MessageService } from './message.service';
import { MessageController } from './message.controller';
import { Message } from 'src/entities/message.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Message])],
  exports: [MessageService],
  controllers: [MessageController],
  providers: [MessageService],
})
export class MessageModule {}
