import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { User } from './user.entity';

export enum State {
  'NOT_FRIEND' = 0, //不是好友
  'APPLYING' = 1, //申请中
  'ISLYING' = 2, //有人申请
  'IS_FRIEND' = 3, //是好友
}

// 好友表
@Entity()
export class Friend {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'userId',
  })
  userId: number;

  @Column({
    name: 'friendId',
  })
  friendId: number;

  @ManyToOne(() => User, (user) => user.friends)
  @JoinColumn({ name: 'userId' })
  user: User; //用户ID

  @ManyToOne(() => User, (user) => user.friends)
  @JoinColumn({ name: 'friendId' })
  friend: User; //好友ID

  @Column({
    type: 'enum',
    enum: State,
    default: State.NOT_FRIEND,
    comment: '好友状态',
  })
  state: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @Column({ nullable: true, comment: '好友昵称' })
  friendName: string;

  @Column({ nullable: true, type: 'timestamp', comment: '最后时间' })
  lastTime: Date;
}
