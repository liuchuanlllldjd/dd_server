import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany,
  BeforeInsert,
} from 'typeorm';
import { Exclude } from 'class-transformer';

import { Friend } from 'src/entities/friend.entity';
export enum Gender {
  'WOMEN' = 0,
  'MAN' = 1,
  'UNKNOWN' = 2,
}

//用户表
@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number; //用户id

  @Column({
    unique: true,
    comment: '用户账号',
  })
  username: string;

  @Column({
    default: 'DD用户',
    comment: '用户昵称',
  })
  nickname: string;

  @Exclude()
  @Column({
    comment: '用户密码',
  })
  password: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @Column({
    type: 'enum',
    enum: Gender,
    default: 2,
    comment: '用户性别',
  })
  gender: number;

  @Column({ nullable: true, comment: '用户邮箱' })
  email: string;

  @Column({ nullable: true, comment: '用户手机号' })
  phone: string;

  @Column({ default: '这个人很懒，还没有个性签名哦～', comment: '用户签名' })
  explain: string;

  @Column({
    default:
      'https://lio-1311014746.cos.ap-shanghai.myqcloud.com/defaultAcr.jpg',
    comment: '头像地址',
    length: 5000,
  })
  imgUrl: string;

  @OneToMany(() => Friend, (friend) => friend.user) // note: we will create author property in the Photo class below
  friends: Friend[];
}
