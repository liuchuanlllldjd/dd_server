import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { User } from './user.entity';
import { Group } from 'src/entities/group.entity';

enum BEEN_READ {
  'UNREAD' = 0, //未读
  'READ' = 1, //已读
}

enum MESSAGE_TYPE {
  MESSAGE = 0, //文字消息
  AUDIO = 1, //音频
  VIDEO = 2, //视频
}

export class GroupMsg {
  @PrimaryGeneratedColumn({ comment: '主键' })
  id: number;

  @ManyToOne(() => User, (user) => user.friends)
  user: User; //用户ID

  @ManyToOne(() => Group, (group) => group.groups)
  Group: Group; //群组ID

  @Column({
    comment: '消息',
  })
  message: string;

  @Column({
    type: 'enum',
    enum: MESSAGE_TYPE,
    default: MESSAGE_TYPE.MESSAGE,
    comment: '消息类型',
  })
  type: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @Column({
    type: 'enum',
    enum: BEEN_READ,
    default: BEEN_READ.UNREAD,
    comment: '消息是否已读',
  })
  state: number;
}
