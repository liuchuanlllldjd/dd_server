import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
} from 'typeorm';
import { User } from './user.entity';

enum BEEN_READ {
  'UNREAD' = 0, //未读
  'READ' = 1, //已读
}

enum MESSAGE_TYPE {
  MESSAGE = 0, //文字消息
  AUDIO = 1, //音频
  VIDEO = 2, //视频
}

//一对一消息表
@Entity()
export class Message {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'userId',
  })
  userId: number;

  @Column({
    name: 'friendId',
  })
  friendId: number;

  @ManyToOne(() => User, (user) => user.friends)
  user: User; //用户ID

  @ManyToOne(() => User, (user) => user.friends)
  friend: User; //好友ID

  @Column({
    comment: '消息',
  })
  message: string;

  @Column({
    type: 'enum',
    enum: MESSAGE_TYPE,
    default: MESSAGE_TYPE.MESSAGE,
    comment: '消息类型',
  })
  type: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @Column({
    type: 'enum',
    enum: BEEN_READ,
    default: BEEN_READ.UNREAD,
    comment: '消息是否已读',
  })
  state: number;
}
