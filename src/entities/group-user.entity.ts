import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { User } from './user.entity';
import { Group } from 'src/entities/group.entity';

export class GroupUser {
  @PrimaryGeneratedColumn({ comment: '群ID' })
  id: number;

  @ManyToOne(() => User, (user) => user.friends)
  user: User; //用户ID

  @ManyToOne(() => Group, (group) => group.groups)
  Group: Group; //群组ID

  @Column({ comment: '群名称' })
  name: string;

  @Column({ comment: '未读消息数量' })
  tip: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @Column({ comment: '是否屏蔽' })
  shield: boolean;
}
