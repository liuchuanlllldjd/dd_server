import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { User } from './user.entity';

export class Group {
  @PrimaryGeneratedColumn({ comment: '群ID' })
  id: number;

  @ManyToOne(() => User, (user) => user.friends)
  user: User; //用户ID

  @Column({ comment: '群名称' })
  name: string;

  @Column({ comment: '公告' })
  notice: string;

  @Column({ comment: '群头像' })
  imgUrl: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @OneToMany(() => Group, (group) => group.user) // note: we will create author property in the Photo class below
  groups: Group[];
}
