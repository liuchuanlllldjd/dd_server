import { IsNotEmpty, IsString, IsByteLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  @IsByteLength(2, 10)
  @ApiProperty({ description: '账号', example: 'lio' })
  username: string;
  @IsNotEmpty()
  @IsString()
  @IsByteLength(6, 10)
  @ApiProperty({ description: '密码', example: 'lio' })
  password: string;
}
export class UpdateUserDto {
  @IsNotEmpty()
  username?: string;
  gender?: number;
  id: number;
}
export class FindUserDto {
  @IsNotEmpty()
  username?: string;
  nickname?: string;
  id?: number;
}

export class FindUserListDto {
  @IsNotEmpty()
  page: number;
  pageSize: number;
  [propName: string]: any;
}
