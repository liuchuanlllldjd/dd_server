import { IsNotEmpty, IsString, IsByteLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { State } from 'src/entities/friend.entity';
export class FindFriendDto {
  @ApiProperty({ description: 'id', example: 1 })
  id?: number;
  @ApiProperty({ description: '用户Id', example: 1 })
  userId?: number;
  @ApiProperty({ description: '好友Id', example: 1 })
  friendId?: number;
  @ApiProperty({ description: '好友状态', example: 1 })
  state?: State;
}
