import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreatedMessageDto {
  @IsNotEmpty()
  @ApiProperty({ description: '当前登录用户id', example: '123' })
  userId: number;

  @IsNotEmpty()
  @ApiProperty({ description: '当前好友id', example: '123' })
  friendId: number;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ description: '消息内容', example: 'text' })
  message: string;
}

export class findAllDto {
  @IsNotEmpty()
  @ApiProperty({ description: '好友id', example: '123' })
  friendId: number;
  @IsNotEmpty()
  @ApiProperty({ description: '跳过多少条', example: '123' })
  skip: number;
  @IsNotEmpty()
  @ApiProperty({ description: '请求多少条', example: '123' })
  take: number;
}

export class findAllMessageDto {
  @IsNotEmpty()
  @ApiProperty({ description: '好友id', example: '123' })
  friendId: number;
  @IsNotEmpty()
  @ApiProperty({ description: '好友id', example: '123' })
  userId: number;
  @IsNotEmpty()
  @ApiProperty({ description: '跳过多少条', example: '123' })
  skip: number;
  @IsNotEmpty()
  @ApiProperty({ description: '请求多少条', example: '123' })
  take: number;
}
