/**
 * 函数运行时区分env 变量不支持
 * @returns
 */
const findConfigModel = () => ({
  // 端口
  port: parseInt(process.env.PORT),
  projectName: process.env.PROJECT_NAME,
  /**
   * 项目部署统一前缀
   */
  prefix: process.env.PREFIX,

  sqlUserName: process.env.SQL_USER_NAME,
  sqlPassword: process.env.SQL_PASSWORD,
  sqlHost: process.env.SQL_HOST,
  sqlDataBase: process.env.SQL_DATA_BASE,
});

/**
 * 默认配置导出
 */
export default findConfigModel;

/**
 * 配置类型
 */
export type ConfigurationType = ReturnType<typeof findConfigModel>;
